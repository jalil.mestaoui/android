package com.example.applicationproject;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.HashMap;

public class FournisseurDAO {

    private DatabaseReference databaseReference;
    public FournisseurDAO()
    {
        FirebaseDatabase db =FirebaseDatabase.getInstance("https://databaseprojet-1ab66-default-rtdb.europe-west1.firebasedatabase.app");
        databaseReference = db.getReference(Fournisseur.class.getSimpleName());
    }
    public Task<Void> add(Fournisseur fournisseur)
    {
        return databaseReference.push().setValue(fournisseur);
    }

    public Task<Void> update(String key, HashMap<String ,Object> hashMap)
    {
        return databaseReference.child(key).updateChildren(hashMap);
    }
    public Task<Void> remove(String key)
    {
        return databaseReference.child(key).removeValue();
    }

    public Query get(String key)
    {
        if(key == null)
        {
            return databaseReference.orderByKey().limitToFirst(8);
        }
        return databaseReference.orderByKey().startAfter(key).limitToFirst(8);
    }

    public Query get()
    {
        return databaseReference;
    }
}




