package com.example.applicationproject;

public class Produit {
    private String description;
    private String nom;
    private double prix;
    private int quantite;
    private String lieuDisponibilite;
    private String uuidUser;

    public Produit(){

    }

    public Produit(String description, String nom, double prix, int quantite, String lieuDisponibilite, String uuidUser) {
        this.description = description;
        this.nom = nom;
        this.prix = prix;
        this.quantite = quantite;
        this.lieuDisponibilite = lieuDisponibilite;
        this.uuidUser = uuidUser;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public String getLieuDisponibilite() {
        return lieuDisponibilite;
    }

    public void setLieuDisponibilite(String lieuDisponibilite) {
        this.lieuDisponibilite = lieuDisponibilite;
    }

    public String getUuidUser() {
        return uuidUser;
    }

    public void setUuidUser(String uuidUser) {
        this.uuidUser = uuidUser;
    }
}

