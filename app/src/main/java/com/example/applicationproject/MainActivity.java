package com.example.applicationproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth auth;
    private Button registerButton;
    private Button loginButton;
    EditText emailInput;
    EditText passwordInput;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        registerButton = findViewById(R.id.registerButton);
        loginButton = findViewById(R.id.loginButton);
        emailInput = findViewById(R.id.emailInput);
        passwordInput =   findViewById(R.id.passwordInput);
        auth = FirebaseAuth.getInstance();

        registerButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
        loginButton.setOnClickListener(view->{
            loginUser();
        });
    }


    private void loginUser(){
        String email_txt = emailInput.getText().toString();
        String password_txt = passwordInput.getText().toString();


        if(TextUtils.isEmpty(email_txt)){
            emailInput.setError( "Le mail doit être rempli");
            emailInput.requestFocus();
        }
        if(TextUtils.isEmpty(password_txt)){
            passwordInput.setError( "Mot de passe doit être rempli");
            passwordInput.requestFocus();
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(email_txt).matches()){
            emailInput.setError("Entrer une adresse email valide");
        }
        if(password_txt.length()<6){
            passwordInput.setError( "Le mot de passe doit être suppérieur à 6 caractères");
            passwordInput.requestFocus();
        }else{
            auth.signInWithEmailAndPassword(email_txt, password_txt).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(MainActivity.this, "Connexion réussi", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(MainActivity.this, Bienvenue.class));
                    }
                    else{
                        Toast.makeText(MainActivity.this, "Login Error" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

    }

}