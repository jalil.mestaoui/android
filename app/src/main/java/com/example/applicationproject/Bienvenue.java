package com.example.applicationproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Bienvenue extends AppCompatActivity {

    private Button AddProduct;
    private Button display;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bienvenue);

        AddProduct = findViewById(R.id.AddProduct);
        display = findViewById(R.id.display);

        AddProduct.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(Bienvenue.this, AddProductActivity.class);
                startActivity(intent);
            }
        });

        display.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(Bienvenue.this, AllProductActivity.class);
                startActivity(intent);
            }
        });
    }


}

