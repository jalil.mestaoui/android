package com.example.applicationproject;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ProduitVH extends RecyclerView.ViewHolder {
    public TextView nomRecycler, descriptionRecycler, prixRecycler, quantiteRecycler, userRecycler, dispoRecycler;
    public ProduitVH(@NonNull View itemView) {
        super(itemView);
        nomRecycler = itemView.findViewById(R.id.nomRecycler);
        descriptionRecycler = itemView.findViewById(R.id.descriptionRecycler);
        prixRecycler = itemView.findViewById(R.id.prixRecycler);
        quantiteRecycler = itemView.findViewById(R.id.quantiteRecycler);
        userRecycler = itemView.findViewById(R.id.userRecycler);
        dispoRecycler = itemView.findViewById(R.id.dispoRecycler);

    }
}
