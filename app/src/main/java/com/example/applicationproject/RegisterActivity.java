package com.example.applicationproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import android.text.TextUtils;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegisterActivity extends AppCompatActivity {



    private FirebaseAuth auth;
    EditText nom;
    EditText prenom;
    EditText email;
    EditText password;
    Button save;
    FournisseurDAO dao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        save = findViewById(R.id.save);
        nom = findViewById(R.id.nom);
        prenom = findViewById(R.id.prenom);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        dao= new FournisseurDAO();

        auth = FirebaseAuth.getInstance();

        save.setOnClickListener(view->{
            createUser();
        });

    }


    private void createUser(){
        String email_txt = email.getText().toString();
        String password_txt = password.getText().toString();
        String nom_txt = nom.getText().toString();
        String prenom_txt = prenom.getText().toString();

        if(TextUtils.isEmpty(nom_txt)){
            nom.setError( "Le nom doit être rempli");
            nom.requestFocus();
        }
        if(TextUtils.isEmpty(nom_txt)){
            prenom.setError( "Le prénom doit être rempli");
            prenom.requestFocus();
        }
        if(TextUtils.isEmpty(nom_txt)){
            email.setError( "Le mail doit être rempli");
            email.requestFocus();
        }
        if(TextUtils.isEmpty(password_txt)){
            password.setError( "Mot de passe doit être rempli");
            password.requestFocus();
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(email_txt).matches()){
            email.setError("Entrer une adresse email valide");
        }
        if(password_txt.length()<6){
            password.setError( "Le mot de passe doit être suppérieur à 6 caractères");
            password.requestFocus();
        }else{
            auth.createUserWithEmailAndPassword(email_txt,password_txt).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        Fournisseur fournisseur = new Fournisseur(email_txt,password_txt,nom_txt,prenom_txt);
                        dao.add(fournisseur).addOnSuccessListener(suc->{
                            Toast.makeText(RegisterActivity.this, "Ajout effectué", Toast.LENGTH_SHORT).show();
                        }).addOnFailureListener(er->{
                            Toast.makeText(RegisterActivity.this, "Erreur : " + er.getMessage(), Toast.LENGTH_SHORT).show();
                        });
                        Toast.makeText(RegisterActivity.this, "Le fournisseur à été enregistré",Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                    }
                    else{
                        Toast.makeText(RegisterActivity.this, "Registration Error : " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

    }

}
