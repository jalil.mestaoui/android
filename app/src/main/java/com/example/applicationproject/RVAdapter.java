package com.example.applicationproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    ArrayList<Produit> list = new ArrayList<>();

    public RVAdapter(Context ctx){
        this.context = ctx;
    }
    public void setItems(ArrayList<Produit> produits){
        list.addAll(produits);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_items,parent, false);
        return new ProduitVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ProduitVH vh = (ProduitVH) holder;
        Produit prod = list.get(position);
        vh.nomRecycler.setText(prod.getNom());
        vh.descriptionRecycler.setText(prod.getDescription());
        vh.prixRecycler.setText(String.valueOf(prod.getPrix()));
        vh.quantiteRecycler.setText(String.valueOf(prod.getQuantite()));
        vh.userRecycler.setText(prod.getUuidUser());
        vh.dispoRecycler.setText(prod.getLieuDisponibilite());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

