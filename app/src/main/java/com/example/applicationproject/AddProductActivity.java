package com.example.applicationproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class AddProductActivity extends AppCompatActivity {
    private Button addProduit;
    EditText nomProduit;
    EditText descriptionProduit;
    EditText prixProduit;
    EditText quantiteProduit;
    EditText lieuProduit;

    ProduitDAO dao;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        addProduit = findViewById(R.id.addProduit);

        nomProduit = findViewById(R.id.nomProduit);
        descriptionProduit = findViewById(R.id.descriptionProduit);
        prixProduit = findViewById(R.id.prixProduit);
        quantiteProduit = findViewById(R.id.quantiteProduit);
        lieuProduit = findViewById(R.id.lieuProduit);

        auth = FirebaseAuth.getInstance();
        dao= new ProduitDAO();

        addProduit.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                addProduit();
            }
        });


    }
    public void addProduit(){
        String descriptionProduit_txt = descriptionProduit.getText().toString();
        String nomProduit_txt = nomProduit.getText().toString();
        //Double.parseDouble(prixProduit.getText().toString());
        String prixProduit_txt = prixProduit.getText().toString();
        //Integer.parseInt(quantiteProduit.getText().toString());
        String quantiteProduit_txt = quantiteProduit.getText().toString();
        String lieuProduit_txt= lieuProduit.getText().toString();

        if(TextUtils.isEmpty(descriptionProduit_txt)){
            descriptionProduit.setError( "Description doit être rempli");
            descriptionProduit.requestFocus();
        }
        else if(TextUtils.isEmpty(nomProduit_txt)){
            nomProduit.setError( "Le nom doit être rempli");
            nomProduit.requestFocus();
        }
        else if(TextUtils.isEmpty(prixProduit_txt)){
            prixProduit.setError( "Prix doit être rempli");
            prixProduit.requestFocus();
        }
        else if(TextUtils.isEmpty(quantiteProduit_txt)){
            quantiteProduit.setError( "Quantité doit être rempli");
            quantiteProduit.requestFocus();
        }
        else if(TextUtils.isEmpty(lieuProduit_txt)){
            lieuProduit.setError( "Lieu doit être rempli");
            lieuProduit.requestFocus();
        }
        else{
            FirebaseUser user = auth.getCurrentUser();
            if(user != null){
                System.out.println("------------------------------ "+ user.getUid());
                int quantiteProduit_int = Integer.parseInt(quantiteProduit.getText().toString());
                Double prixProduit_double= Double.parseDouble(prixProduit.getText().toString());
                Produit produit = new Produit(descriptionProduit_txt, nomProduit_txt, prixProduit_double, quantiteProduit_int, lieuProduit_txt, user.getUid());
                dao.add(produit).addOnSuccessListener(suc->{
                    Toast.makeText(AddProductActivity.this, "Ajout produit effectué", Toast.LENGTH_SHORT).show();
                    startActivity( new Intent(AddProductActivity.this, AllProductActivity.class));
                }).addOnFailureListener(er->{
                    Toast.makeText(AddProductActivity.this, "Erreur : " + er.getMessage(), Toast.LENGTH_SHORT).show();
                });
            }

        }
    }
}